entity Client {
    name String required
    startDate LocalDate
    endDate LocalDate
    status Status required
}
entity Tax {
    clientId Long required
    name String required
    code String required
    status Status
}
entity Currency {
    clientId Long required
	name String required
    code String required
    status Status
}
entity BillingType {
    clientId Long required
	name String required
    type String
    status Status
}
entity BillingPeriod {
    clientId Long required
	name String required
    type String
    status Status
}
entity CustomerCategory {
    clientId Long required
	name String required maxlength(32)
    billCreditLimit BigDecimal required
    creditLimit BigDecimal required
    status Status required
}
entity Customer {
    clientId Long required
	name String required maxlength(32)
    company String required
    status Status required
}
entity CustomerContact {
    address1 String required
    address2 String
    city String required maxlength(32)
    state String required maxlength(15)
    country String required maxlength(15)
    zipcode String required maxlength(12)
    status Status required
}
entity CustomerBillingInfo {
    tin String
    gst String
    pan String
    billCreditLimit BigDecimal
    creditLimit BigDecimal
    status Status required
}
entity ItemType {
    clientId Long required
    name String required
    status Status required
}
entity Unit {
    clientId Long required
    name String required maxlength(12)
    code String required maxlength(12)
    status Status required
}
entity Item {
    clientId Long required
	name String required maxlength(32)
    code String required maxlength(16)
    description String required maxlength(300)
    status Status required
}
entity ItemPrice {
    price BigDecimal required
    status Status required
}
entity ItemPriceHistory {
    itemId Long required
    price BigDecimal required
    startDate LocalDate required
    endDate LocalDate
}
entity Invoice {
    date LocalDate required
    dueDate LocalDate required
    total BigDecimal required
    balance BigDecimal
    carriedBalance BigDecimal
    discount BigDecimal
    isPercentage Boolean
    notes String
    status Status required
}
entity InvoiceLine {
    quantity Double required
    price BigDecimal required
    discount BigDecimal
    isPercentage Boolean
}
entity PaymentMethod {
    clientId Long required
    name String required maxlength(16)
    status Status required
}
entity Payment {
    amount BigDecimal required
    date LocalDate required
    status Status required
}
entity InvoicePayment {
    invoiceId Long 	required
    status Status required
}
relationship OneToOne  {
    InvoicePayment{payment} to Payment
}
relationship OneToOne {
    Payment{customer} to Customer
	Payment{paymentMethod} to PaymentMethod
}
relationship OneToOne {
    Invoice{customer} to Customer
    InvoiceLine{item} to Item
}
relationship OneToMany {
    Invoice{invoiceLine} to InvoiceLine
}
relationship OneToOne {
    Item{itemPrice} to ItemPrice
	Item{itemType} to ItemType
    Item{unit} to Unit
    Item{tax} to Tax
}
relationship OneToOne {
    Customer{customerCategory} to CustomerCategory
	Customer{customerBillingInfo} to CustomerBillingInfo
	Customer{currency} to Currency
	Customer{billingType} to BillingType
    Customer{billingPeriod} to BillingPeriod
}
relationship OneToMany {
    Customer{customerContact} to CustomerContact
}
enum Status {
    ACTIVE, DEACTIVE
}
paginate all with pagination
dto all with mapstruct
service all with serviceImpl
