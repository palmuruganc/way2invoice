package com.way2invoice.bms.service.mapper;


import com.way2invoice.bms.domain.*;
import com.way2invoice.bms.service.dto.InvoicePaymentDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link InvoicePayment} and its DTO {@link InvoicePaymentDTO}.
 */
@Mapper(componentModel = "spring", uses = {PaymentMapper.class})
public interface InvoicePaymentMapper extends EntityMapper<InvoicePaymentDTO, InvoicePayment> {

    @Mapping(source = "payment.id", target = "paymentId")
    InvoicePaymentDTO toDto(InvoicePayment invoicePayment);

    @Mapping(source = "paymentId", target = "payment")
    InvoicePayment toEntity(InvoicePaymentDTO invoicePaymentDTO);

    default InvoicePayment fromId(Long id) {
        if (id == null) {
            return null;
        }
        InvoicePayment invoicePayment = new InvoicePayment();
        invoicePayment.setId(id);
        return invoicePayment;
    }
}
