package com.way2invoice.bms.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import com.way2invoice.bms.domain.enumeration.Status;

/**
 * A DTO for the {@link com.way2invoice.bms.domain.Customer} entity.
 */
public class CustomerDTO implements Serializable {
    
    private Long id;

    @NotNull
    private Long clientId;

    @NotNull
    @Size(max = 32)
    private String name;

    @NotNull
    private String company;

    @NotNull
    private Status status;


    private Long customerCategoryId;

    private Long customerBillingInfoId;

    private Long currencyId;

    private Long billingTypeId;

    private Long billingPeriodId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Long getCustomerCategoryId() {
        return customerCategoryId;
    }

    public void setCustomerCategoryId(Long customerCategoryId) {
        this.customerCategoryId = customerCategoryId;
    }

    public Long getCustomerBillingInfoId() {
        return customerBillingInfoId;
    }

    public void setCustomerBillingInfoId(Long customerBillingInfoId) {
        this.customerBillingInfoId = customerBillingInfoId;
    }

    public Long getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(Long currencyId) {
        this.currencyId = currencyId;
    }

    public Long getBillingTypeId() {
        return billingTypeId;
    }

    public void setBillingTypeId(Long billingTypeId) {
        this.billingTypeId = billingTypeId;
    }

    public Long getBillingPeriodId() {
        return billingPeriodId;
    }

    public void setBillingPeriodId(Long billingPeriodId) {
        this.billingPeriodId = billingPeriodId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CustomerDTO)) {
            return false;
        }

        return id != null && id.equals(((CustomerDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CustomerDTO{" +
            "id=" + getId() +
            ", clientId=" + getClientId() +
            ", name='" + getName() + "'" +
            ", company='" + getCompany() + "'" +
            ", status='" + getStatus() + "'" +
            ", customerCategoryId=" + getCustomerCategoryId() +
            ", customerBillingInfoId=" + getCustomerBillingInfoId() +
            ", currencyId=" + getCurrencyId() +
            ", billingTypeId=" + getBillingTypeId() +
            ", billingPeriodId=" + getBillingPeriodId() +
            "}";
    }
}
