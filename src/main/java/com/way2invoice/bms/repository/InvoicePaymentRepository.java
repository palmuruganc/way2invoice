package com.way2invoice.bms.repository;

import com.way2invoice.bms.domain.InvoicePayment;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the InvoicePayment entity.
 */
@SuppressWarnings("unused")
@Repository
public interface InvoicePaymentRepository extends JpaRepository<InvoicePayment, Long> {
}
